﻿(function (data) {

    var database = require('./database.js');

    data.addEntry = function(entry, next) {
    
        database.getDb(function (err, db) {
            if (err) { next(err, null); }
            else {
            
                db.daily_entries.insert(entry, function (err) {
                    if (err) {
                        next(err, null);
                    } else {
                        next(null, entry);
                    }
                });
            }
        });

    };
    
    data.removeEntry = function (entryId, next) {
        
        var ObjectId = require('mongodb').ObjectID;

        database.getDb(function (err, db) {
            if (err) { next(err, null); }
            else {
                
                var result = db.daily_entries.remove({ "_id": ObjectId(entryId) }, { w: 0 }, function (err, result) {
                    if (err) { next(err, null); }
                    else {
                        next(null, result);
                    }        
                });  // true = limit delete to 1 doc

            }
        });

    };

    data.getEntriesForToday = function (next) {
        
        var start = new Date(), end = new Date();

        start.setHours(0, 0, 0);
        end.setHours(23, 59, 59);

        database.getDb(function (err, db) {
            if (err) { next(err, null); }
            else {
                db.daily_entries.find({ "time": { $gte: start, $lte: end } }).sort({ time: -1 }).toArray(function (err, results) {

                    if (err) { next(err, null); }
                    else {
                        var returnValues = [];
                        
                        results.map(function (item) {
                            returnValues.push(item);
                        });

                        next(null, returnValues);
                    }

                });
            }
        });

    };

    data.getEntriesForWeek = function (timezone_offset, next) {
        
        var util = require('../utilities');

        database.getDb(function (err, db) {
            
            if (err) { next(err, null); }
            else {
                
                var curr = new Date(); // get current date
                var first = curr.getDate() - (curr.getDay()); // First day = day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6
                
                var start = new Date(curr.setDate(first));
                var end = new Date(curr.setDate(last));
                
                var tz_milli = timezone_offset * (60 * 60 * 1000);
                
                start.setHours(0, 0, 0);
                end.setHours(23, 59, 59);
                
                db.daily_entries.aggregate(
                    [
                        { $match: { time: { $gt: start, $lte: end } } },
                        { $group: { _id: { year: { $year: [{ $subtract: ["$time", tz_milli] }] }, month: { $month: [{ $subtract: ["$time", tz_milli] }] }, day: { $dayOfMonth: [{ $subtract: ["$time", tz_milli] }] } }, total_calories: { $sum : "$calories" }, total: { $sum: 1 } } },
                        { $sort: { _id: 1 } }
                    ],
                    function (err, results) {
                        var returnValues = [];
                        
                        results.map(function (item) {
                            item.date = new Date(item._id.year, item._id.month - 1, item._id.day);
                            returnValues.push(item);
                        });
                        
                        next(null, returnValues);
                    }
                );

            }

        });

    };

})(module.exports);