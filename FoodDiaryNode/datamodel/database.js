﻿(function (database) {

    var mongodb = require("mongodb");
    var mongoUrl = "mongodb://localhost:27017/fooddiary";
    var connectDb;
    var theDb = null;

    database.getDb = function (next) {
        
        if (process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
            mongoUrl = "mongodb://" + process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
                process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
                process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
                process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
                process.env.OPENSHIFT_APP_NAME;
        }
        
        if (!theDb) {
            
            console.log("need new connection.  connecting to: " + mongoUrl);
            
            mongodb.MongoClient.connect(mongoUrl, function (err, db) {

                if (err) {
                    next(err, null);
                } else {
                    
                    theDb = {
                        db: db,
                        foods: db.collection("foods"),
                        daily_entries: db.collection("daily_entries")
                    };
                    
                    next(null, theDb);

                }

            });

        } else {
            console.log("used an existing connection");
            next(null, theDb);
        }
    };

    database.operation = function (next, oper) {

        this.getDb(function (err, db) {
            
            if (err) { next(err, null); }
            else {
                
                if (typeof oper === 'function') {
                    oper(db);
                }

            }

        });

    };

})(module.exports);