﻿(function (data) {

    var database = require('./database.js');

    data.addFood = function (food, next) {
        
        database.operation(next, function (db) {

            db.foods.insert(food, function (err) {
                if (err) { next(err, null); } 
                else { next(null, food); }
            });

        });

    };
    
    data.updateFood = function (foodId, food, next) {
        
        var ObjectId = require('mongodb').ObjectID;
        
        database.operation(next, function (db) {

            db.foods.update({ _id: ObjectId(foodId) }, { $set: { name: food.name, type: food.type, calories: food.calories } }, function (err, result) {
                if (err) { next(err, null); }
                else {
                    next(null, result);
                }
            });

        });

    };
    
    data.removeFood = function (foodId, next) {
        
        var ObjectId = require('mongodb').ObjectID;
        
        database.operation(next, function (db) {

            db.foods.remove({ "_id": ObjectId(foodId) }, { w: 0 }, function (err, result) {
                if (err) { next(err, null); }
                else {
                    next(null, result);
                }
            });  // true = limit delete to 1 doc

        });

    };
    
    data.getFood = function (foodId, next) {
        var ObjectId = require('mongodb').ObjectID;

        database.operation(next, function (db) {
            db.foods.findOne({ "_id": ObjectId(foodId) }, function (err, result) {
                if (err) { next(err, null); }
                else {
                    next(null, result);
                }       
            });
        });  
    };
    
    data.getFoods = function (options, next) {
        
        var defaults = {
            perPage: 15,
            page: 1,
            itemOffset: 0
        };
        
        var extend = require('extend');
        var opts = extend(true, defaults, options);
        
        opts.itemOffset = (opts.page - 1) * opts.perPage;
            
        database.operation(next, function (db) {

            var foods = db.foods.find();
            var totalCount = 0;

            foods.count(function (err, count) { totalCount = count; });  // not sure if there's a better way here?

            foods.limit(opts.perPage).skip(opts.itemOffset).toArray(function (err, results) {
                next(null, {
                    'results': results,
                    'paging': {
                        totalItems: totalCount,
                        totalPages: Math.ceil(totalCount / opts.perPage),
                        page: opts.page,
                        perPage: opts.perPage
                    }
                });
            });

        });
          
    };

    data.getFoodTitles = function (title, next) {
        
        database.operation(next, function (db) {

            db.foods.find({ name: { $regex: title, $options: '-i' } }).toArray(function (err, results) {
                
                if (err) { next(err, null); }
                else {
                    next(null, results.map(function (item) {
                        return { label: item.name, value: item.calories.toString() };
                    }));
                }

            });

        });

    };

})(module.exports);