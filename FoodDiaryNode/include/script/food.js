﻿
Food = {

    Load: function () {

        $('table.foods').on('click', '.delete-food', function () {
            Food.Delete(Food.RowFoodId(this));
        });

        $('table.foods').on('click', '.edit-food', function () {
            Food.Edit.InitInline(Food.RowFoodId(this));
        });

    },
    
    RowFoodId: function (row) {
        return $(row).parents('tr').data('food-id');
    },

    Edit: {

        InitInline: function (id) {
            // grab partial from the server w/ food details
            // partial should be a table row that we replace
            // unsure if we should hide/show the original after update

            $.ajax({
                url: '/food/edit',
                type: 'GET',
                data: { 'food_id': id },
                success: function (data) {
                    
                    var $row = $('table.foods').find('tr[data-food-id=' + id + ']');
                    var $origRow = $row.clone(true);

                    $row.replaceWith(data);

                    $('.save-food-edit').click(function () {
                        
                        var _id = $(this).data('food-id');

                        $.ajax({
                            url: '/food/edit',
                            type: 'POST',
                            data: {
                                'food_id': _id,
                                'name': $('#food_name').val(),
                                'type': 'fats',
                                'calories': parseInt($('#food_calories').val())
                            },
                            success: function (data) {
                                if (data.success) {
                                    $('table.foods').find('tr[data-food-id=' + id + ']').replaceWith(data.updated_row);
                                    //$('#food_name').focus();
                                }
                            }
                        });

                    });

                    $('.cancel-edit').click(function () {
                        $('table.foods').find('tr[data-food-id=' + id + ']').replaceWith($origRow);
                    });
                }
            });
        },

        Update: function (id) {
        }

    },

    Delete: function (id) {
            
        $.ajax({
            url: '/food/delete',
            type: 'DELETE',
            data: { 'food_id': id },
            success: function (data) {

                if (data.success) {
                    Food.RemoveRow(id); // remove the offending row
                } else {
                    // show message
                }

            }
        });

    },

    RemoveRow: function (id) {
        
        $('table.foods').find('tr[data-food-id=' + id + ']').remove();

    }

};

$(function () {
    
    Food.Load();

});