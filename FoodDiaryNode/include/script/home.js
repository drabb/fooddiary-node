﻿
Home = {
    
    ViewMap: {
        'new_entry': 'Home.DailyEntry.Load',
        'new_food': 'Home.NewFood.Load'
    },
    
    Load: function () {
        
        var currentView = $('#view-name').val();
        var loadFunc = Home.ViewMap[currentView];
        
        FoodDiary.ExecuteFunction(loadFunc, window);

    },
    
    DailyEntry: {
        
        Load: function () {
            var $foodName = $('#food_name');
            
            $('#new_food_type').change(function () { 
                $(this).removeClass('placeholder');
            });
            
            // wire up the autocomplete
            $foodName.autocomplete({
                source: '/food/search',
                focus: function (e, ui) { Home.DailyEntry.SetFoodName(e, ui, false); },
                select: function (e, ui) { Home.DailyEntry.SetFoodName(e, ui, true); }
            });
            
            $foodName.focus();  // don't touch that mouse
            
            var currentTime = new Date();

            // wire up the date/time selector
            $('#food_time').datetimepicker({
                timeFormat: "hh:mm tt",
                stepMinute: 5,
                hour: currentTime.getHours(),  // start the slider at 'now'
                minute: currentTime.getMinutes(),
                controlType: FoodDiary.IsTouchDevice() ? 'select' : 'slider',
                onSelect: function (text, dpInstance) {
                    $('#food_time_val').val(new Date(text).toString());
                }
            });
            
            $('.add-new-food').click(Home.DailyEntry.ToggleNewFood);
            $('.show-this-week').click(Home.DailyEntry.LoadDailyTotals);
            
            Home.CalorieGoal.Init();  // setup calorie goal value & dialog

            // wire up the slider (daily goal range thingy)
            var dailyCalories = $('#daily-calories').val();
            
            $('.daily-goal-slider').slider({
                disabled: true,
                range: 'min',
                min: 0,
                max: Home.CalorieGoal.Current,  // this value must be inited prior ^
                value: parseInt(dailyCalories)
            });

            Home.DailyEntry.InitValidation();
            
            var stillHovering = false;

            $('.daily-entries tbody tr').hover(  // getting multiple row highlighting?  probably have to track row id
                function () {
                    var $currentRow = $(this);
                    stillHovering = true;
                    setTimeout(function () {
                        if (stillHovering) {
                            $currentRow.addClass('can-edit');
                            //var top = $currentRow.offsetTop();

                        }
                    }, 2000);
                },
                function () {
                    stillHovering = false;
                    $(this).removeClass('can-edit');
                }
            );

            $('.daily-entries tbody tr').click(function (e) { // refactor this into a quick method
                if ($(this).hasClass('can-edit')) {
                    var entryId = $(this).find('input[type=hidden]').val();

                    $.ajax({
                        url: '/entry/delete',
                        type: 'DELETE',
                        data: { 'entry_id': entryId },
                        success: function (data) {
                            
                            alert('bulsh');
                            // remove the offending row
                        }
                    });
                }
            });

        },
        
        IsAddFoodOpen: function () {
            return $('.add-new-food').attr('src').indexOf('arrow-up') > 0;
        },
        
        InitValidation: function () {
            $('#daily-entry-form').validate({  // validation for main form
                rules: {
                    food_name: { required: true },
                    new_food_calories: {
                        required: {
                            depends: function (element) { return Home.DailyEntry.IsAddFoodOpen(); }
                        }
                    },
                    new_food_type: {
                        required: {
                            depends: function (element) { return Home.DailyEntry.IsAddFoodOpen(); }
                        }
                    }
                }
            });
        },

        SetFoodName: function (e, ui, setHidden) {
            e.preventDefault();

            // TODO: add auto-open new food if no results ?

            $('#food_name').val(ui.item.label);

            if (setHidden) {
                $('#food_calories').val(ui.item.value);
            }
        },

        ToggleNewFood: function () {
            var $icon = $(this);
            var $newFoodForm = $('.new-food-entry');
            var src = $icon.attr('src');
            
            if (src.indexOf('plus') > 0) {
                $newFoodForm.slideDown(500);
                $icon.attr('src', '../image/arrow-up.svg');
            } else {
                $newFoodForm.slideUp(500, function () {
                    $icon.attr('src', '../image/plus.svg');
                    $newFoodForm.hide();
                    
                    $('#new_food_calories').val('');  // clear form as well
                    $('#new_food_type').prop('selectedIndex', 0);
                });
            }
        },

        LoadDailyTotals: function () {

            var $wrapper = $('.daily-totals-wrapper .table-wrapper');
            var $showHideLink = $('.show-this-week');
            
            if (!$wrapper.is(':visible')) {
                
                $.ajax({
                    url: '/entry/weekly',
                    type: 'GET',
                    data: { 'client_date': new Date().toDateString() },
                    success: function (data) {
                        if ($wrapper.length === 1) {
                            $wrapper.replaceWith(data);
                        } else {
                            $('.show-this-week').after(data);
                        }
                        
                        // change link text
                        $showHideLink.html('Hide Daily Totals');
                    }
                });

                //$.get('entry/weekly', function (data) {
                    
                //    // shove the returned html table into a div, slide out
                //    // basically the idea here is to not load these results until
                //    // user requested.
                    
                //    if ($wrapper.length === 1) {
                //        $wrapper.replaceWith(data);
                //    } else {
                //        $('.show-this-week').after(data);
                //    }
                    
                //    // change link text
                //    $showHideLink.html('Hide Daily Totals');
                //});

            } else {

                $wrapper.addClass('hidden');
                $showHideLink.html('Show Daily Totals');
                $('.daily-totals').slideUp();

            }

        },
    },
    
    NewFood: {
        Load: function () {
            Home.NewFood.InitValidation();
        },

        InitValidation: function () {
            
            $('#new-food-form').validate({  // validation for main form
                rules: {
                    name: { required: true },
                    calories: { required: true },
                    type: { required: true }
                }
            });

        }
    },

    CalorieGoal: {

        Dialog: null,
        CookieKey: 'calorie_goal',
        Current: 0,
        Default: 1600,
        $CalorieGoal: $('#calorie_goal'),

        Init: function () {
            
            Home.CalorieGoal.Current = $.cookie(Home.CalorieGoal.CookieKey);
            
            if (typeof (Home.CalorieGoal.Current) === 'undefined') {  // don't have a goal, set a default one
                Home.CalorieGoal.Current = Home.CalorieGoal.Default;
                $.cookie(Home.CalorieGoal.CookieKey, Home.CalorieGoal.Current);
            }

            Home.CalorieGoal.InitDialog();
        },

        InitDialog: function () {
            
            Home.CalorieGoal.Dialog = $('.calorie-goal-form').dialog({
                autoOpen: false,
                modal: true,
                title: 'Update Goal',
                buttons: {
                    'Update': Home.CalorieGoal.Update,
                    'Cancel': function () { Home.CalorieGoal.Dialog.dialog('close'); }
                }
            });
            
            $('.update-goal').off('click').on('click', function () {
                Home.CalorieGoal.Dialog.dialog('open');
            });

            Home.CalorieGoal.$CalorieGoal.val(Home.CalorieGoal.Current);

        },

        Update: function () {

            var goal = parseInt(Home.CalorieGoal.$CalorieGoal.val());

            $.cookie(Home.CalorieGoal.CookieKey, goal);

            Home.CalorieGoal.Dialog.dialog('close');

            location.reload(true);  // hack? run to server to update the view(s)

            //TODO: possibly make a method that updates the UI with new goals?
            // not sure it's worth it, cause this feature will be seldom used

        }

    }

};

$(function () {
    
    Home.Load();

});