﻿var FoodDiary = (function () {
    
    //var privateThing = function (text) {
    //    console.log(text);
    //};
    
    return {
        
        ExecuteFunction: function (name, context) {
            //http://stackoverflow.com/questions/359788
            
            var args = Array.prototype.slice.call(arguments, 2);
            var namespaces = name.split('.');
            var func = namespaces.pop();
            for (var i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
            }
            return context[func].apply(context, args);
        },
        
        IsTouchDevice: function () {
            return !!('ontouchstart' in window);
        }

    };

})();