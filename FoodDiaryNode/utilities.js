﻿(function (utilities) {
    
    utilities.convertDateToUTC = function (date) {  // returns passed in Date object to UTC time

        return new Date(
            date.getUTCFullYear(), 
            date.getUTCMonth(), 
            date.getUTCDate(), 
            date.getUTCHours(), 
            date.getUTCMinutes(), 
            date.getUTCSeconds()
        );

    };

    utilities.readCookie = function (req, name, defaultIfNotSet) {
        return typeof (req.cookies[name]) === 'undefined' ? defaultIfNotSet : req.cookies[name];
    };

})(module.exports);