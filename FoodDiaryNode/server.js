﻿
var http = require('http');
var express = require('express');
var fs = require('fs');

var foodDiaryApp = function () {
  
    self = this;

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function () {
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;  // grab from OpenShift if available
        self.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
        
        if (typeof self.ipaddress === "undefined") {
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        }
    };
    
    /**
     *  Populate the cache.
     */
    //self.populateCache = function () {
    //    if (typeof self.zcache === "undefined") {
    //        self.zcache = { 'index.html': '' };
    //    }
        
    //    //  Local cache for static content.
    //    self.zcache['index.html'] = fs.readFileSync('./index.html');
    //};
    
    //self.cache_get = function (key) { return self.zcache[key]; };  // Retrieve entry (content) from cache.

    self.terminator = function (sig) {
        if (typeof sig === "string") {
            console.log('%s: Received %s - terminating sample app ...', Date(Date.now()), sig);
            process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()));
    };
    
    self.setupTerminationHandlers = function () {
        //  Process on exit and signals.
        process.on('exit', function () { self.terminator(); });
        
        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
         'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function (element, index, array) {
            process.on(element, function () { self.terminator(element); });
        });
    };
    
    self.initializeControllers = function () {
        var controllers = require("./controllers");
        controllers.init(self.app);
    };

    self.initializeServer = function () {
        
        self.app = express();
        
        //  Add handlers for the app (from the routes).
        //for (var r in self.routes) {
        //    self.app.get(r, self.routes[r]);
        //}
        
        self.app.set('view engine', 'vash');  // set view engine
        self.app.use(express.static(__dirname + "/include"));  // path to our static resources
        
        self.app.use(express.json());  // json encoded bodies
        self.app.use(express.urlencoded());  // url encoded bodies
        
        self.app.use(express.cookieParser());

        self.initializeControllers();
        
        //TODO: seems like adding these make requests take forever?

        self.app.use(function (req, resp) {
            resp.render('404', {
                title: 'File Not Found',
                view: '404'
            });
        });
        
        self.app.use(function (error, req, resp, next) {
            resp.render('error', {
                title: 'Error Occurred',
                error: error,
                view: 'error'
            });
        });

    };

    self.initialize = function () {
        self.setupVariables();
        //self.populateCache();
        self.setupTerminationHandlers();
        
        // Create the express server and routes.
        self.initializeServer();
    };

    self.start = function () {
        //  Start the app on the specific interface (and port).
        self.app.listen(self.port, self.ipaddress, function () {
            console.log('%s: Node server started on %s:%d ...', Date(Date.now()), self.ipaddress, self.port);
        });
    };
    
};

var app = new foodDiaryApp();

app.initialize();
app.start();