﻿(function (controllers) {
    
    var homeController = require("./homeController.js");
    var foodController = require("./foodController");
    
    controllers.init = function (app) {
        homeController.init(app);
        foodController.init(app);
    };

})(module.exports);