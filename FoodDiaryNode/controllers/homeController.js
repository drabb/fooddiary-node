﻿(function (homeController) {

    homeController.init = function (app) {
    
        app.get("/", function (req, resp) {
            
            resp.redirect('/addentry');

        });

        app.get("/doolli", function (req, resp) {

            var dool = require('../doolli');
            dool.app_key = "F7CE071E-F243-47BC-859A-13FAC78AB53A";

            var dbId = 3827;
            var itemId = 1209091;

            //dool.database.getById(dbId, function (err, result) {

            //    resp.json(result);

            //});
            
            var title = req.query.title;
            
            // TODO: what if we name our field with just a number?
            // or what about escaped vals in name &amp;
            
            // maybe just tell sdk/wrapper what to expect?  name vs. id ... bah

            var item = {
                "Name": [ title !== undefined ? title : "From the API"],
                "Date": ["2015"],
                "Dish Choices": ["Chicken"],
                "Category": ["Sub Category A"],
                "Text": ["Random free-form text goes in here"],
                "DatePlugin": ["12/02/2012"],
                "1234": ["3"]
            }

            dool.item.add(dbId, item, function (err, result) { 
                
                resp.json(result);

            });

        });
        
        app.get('/addfood', function (req, resp) {
           
            resp.render('home/new_food', {
                title: 'Add Food',
                view: 'new_food'
            });
             
        });

        app.post("/addfood", function (req, resp) {
        
            var data = require("../datamodel/food");
            
            var newFood = {
                name: req.body.name,
                type: req.body.type,
                calories: req.body.calories
            };
            
            data.addFood(newFood, function (err, result) {
                
                if (err) {
                    resp.send(500, "Failed to add new food to data store.  Error: " + err.message);
                }
                else {
                    
                    // check the xhr flag for ajax requests, different response if so
                    if (req.xhr) {
                        //resp.set("Content-Type", "application/json");
                        //resp.send(200, siteToInsert);
                    } else {
                        resp.redirect('/addfood');
                    }

                }
                 
            });

        });
        
        app.get('/addentry', function (req, resp) {
            
            var data = require("../datamodel/daily_entry");
            var util = require('../utilities');
             
            var goal = parseInt(util.readCookie(req, 'calorie_goal', 1600));

            data.getEntriesForToday(function (err, results) {
                
                resp.render('home/new_entry', {
                    title: 'Add Daily Food',
                    view: 'new_entry',
                    daily_entries: results,
                    calorie_goal: goal
                });

            });
             
        });
        
        app.post("/addentry", function (req, resp) {
        
            var data = require("../datamodel/daily_entry");
            var post = req.body;

            var time = (post.food_time_val === "") ? new Date() : new Date(post.food_time_val);
            
            var addingFood = (post.food_calories == -1 && post.new_food_calories !== '');

            var newEntry = {
                name: post.food_name,
                time: time,
                calories: parseInt(addingFood ? post.new_food_calories : post.food_calories)
            };

            data.addEntry(newEntry, function (err, result) {

                if (err) {
                    resp.send(500, "Bad things");
                }
                else {
                    
                    if (addingFood) {  // add a food entry too
                        var foodData = require("../datamodel/food");

                        var newFood = {
                            name: post.food_name,
                            type: post.new_food_type,
                            calories: parseInt(post.new_food_calories)
                        };

                        foodData.addFood(newFood, function (err, result) {
                            if (err) { resp.send(500, "Failed to add new food to data store.  Error: " + err.message); }
                            else { resp.redirect('/addentry'); }
                        });

                    }
                    else {
                    
                        if (req.xhr) {  // this was an ajax request, send json
                            resp.json({
                                'success': true
                            });
                        } else {
                            resp.redirect('/addentry');
                        }

                    }

                }

            });

        });
        
        app.delete("/entry/delete", function (req, resp) {
            
            var data = require("../datamodel/daily_entry");

            data.removeEntry(req.body.entry_id, function (err, results) {
                
                //TODO: actually handle an error
                resp.json({
                    'success': err === null
                });

            });

        });
        
        app.get("/entry/today", function (req, resp) {

            var data = require("../datamodel/daily_entry");

            data.getEntriesForToday(function (err, results) {

                resp.json(results);

            });
        });
        
        app.get("/entry/weekly", function (req, resp){
        
            var data = require("../datamodel/daily_entry");
            var util = require('../utilities');
            
            var timezone_offset = 4;

            data.getEntriesForWeek(timezone_offset, function (err, results) {

                resp.render('home/daily_totals_list', {
                    title: 'Weekly Calorie Totals by Day',
                    view: 'daily_totals_entry',
                    daily_totals: results,
                    calorie_goal: parseInt(util.readCookie(req, 'calorie_goal', 1600))
                });

            });
        });

        app.get("/food/search", function (req, resp) {
        
            var data = require("../datamodel/food");

            data.getFoodTitles(req.query.term, function (err, results) {

                resp.json(results);

            });
        });
    };

})(module.exports);