﻿(function (foodController) {
    
    foodController.loadFoods = function (pageNum, resp) {

        var _perPage = 15;
        var data = require("../datamodel/food");
        
        data.getFoods({ perPage: _perPage, page: pageNum }, function (err, results) {
            
            if (err) {
                resp.send(500, "Bad things");
            }
            else {
                resp.render('food/index', {
                    title: 'Foods',
                    view: 'foods',
                    foods: results.results,
                    paging: results.paging,
                    view_width: 'wide'
                });
            }
                    
        });

    };

    foodController.init = function (app) {
        
        app.get('/food', function (req, resp) { foodController.loadFoods(1, resp); });
        
        app.get('/food/page/:num', function (req, resp) {
            
            var pageNum = parseInt(req.params.num);
            var data = require("../datamodel/food");
            
            foodController.loadFoods(pageNum, resp);

        });
        
        app.get("/food/edit", function (req, resp) {
           
            var data = require("../datamodel/food");
            
            data.getFood(req.query.food_id, function (err, result) {
                
                if (err) {
                    resp.json({
                        'success': false
                    });
                }
                else {
                    resp.render('food/inline_edit_row', {
                        food: result
                    });
                }

            });
             
        });
        
        app.post("/food/edit", function (req, resp) {
            
            var data = require("../datamodel/food");
            
            var post = req.body;
            
            var food = {
                name: post.name,
                type: post.type,
                calories: parseInt(post.calories)
            };

            data.updateFood(post.food_id, food, function (err, result) {
                
                food._id = post.food_id;

                resp.render('food/inline_row', food, function (err, result) {
                    resp.json({
                        'success': err === null,
                        'updated_row': result
                    });
                });

            });

        });

        app.delete("/food/delete", function (req, resp) {
            
            var data = require("../datamodel/food");
            
            data.removeFood(req.body.food_id, function (err, results) {
                
                resp.json({
                    'success': err === null
                });

            });

        });

    };

})(module.exports);